SAMPLE APP ONE

1) A sample app that displays the current date-time
	- Has a view that shows the current date and time
	- date and time field are aligned so its always the same distance from the top
	- the device time updates every second
2) A button titled "Change Time zone" which opens a new view to change the time zone
	- displays fullscreen on a phone, in a dialog on a tablet
	- display a list of available time zones in a list view showing either region/city or standard time difference (EST/GMT+1)
	- currently selected time zone should be indicated with a color #87CEFA
	- new Time zone should be displayed when user moves back to the main view or selects away from the dialog
	- current timezone displayed below date and time
	- selecting "Change Time zone" will reflect the currently selected time-zone