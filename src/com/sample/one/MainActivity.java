package com.sample.one;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity implements OnItemClickListener {

	private DateTimeView dateTimeView;
	private int pressedColor, defaultColor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		defaultColor = getResources().getColor(R.color.default_color);
		pressedColor = getResources().getColor(R.color.pressed_color);

		dateTimeView = (DateTimeView) findViewById(R.id.dateTimeView1);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long arg3) {
		CustomAdapter adapter = (CustomAdapter) parent.getAdapter();
		adapter.setSelectedIndex(position);
		adapter.notifyDataSetChanged();
		dateTimeView
				.setTimeZone((String) parent.getAdapter().getItem(position));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Displays a Dialog used to select a time zone
	 */
	private void showChangeTimeZoneDialog(){
		ArrayList<String> timeZones = dateTimeView
				.getAvailableTimeZones();
		int startIndex = Collections.binarySearch(timeZones,
				dateTimeView.getTimeZone());
		CustomAdapter adapter = new CustomAdapter(MainActivity.this,
				android.R.layout.simple_list_item_1, timeZones);
		Dialog chooseTimeZoneDialog = new Dialog(MainActivity.this);
		chooseTimeZoneDialog
				.requestWindowFeature(Window.FEATURE_NO_TITLE);
		chooseTimeZoneDialog.setContentView(R.layout.choose_timezone);
		ListView listView = (ListView) chooseTimeZoneDialog
				.findViewById(R.id.listView1);
		adapter.setSelectedIndex(startIndex);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(MainActivity.this);
		listView.setSelection(startIndex);
		chooseTimeZoneDialog.show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_changeTime:
			showChangeTimeZoneDialog();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private class CustomAdapter extends ArrayAdapter<String> {

		public CustomAdapter(Context context, int resource, List<String> objects) {
			super(context, resource, objects);
		}

		private int selectedIndex;

		/**
		 * set the index of the default selected item in this adapter
		 * 
		 * @param index
		 *            - the index of the selected item
		 */
		public void setSelectedIndex(int index) {
			this.selectedIndex = index;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = super.getView(position, convertView, parent);
			if (v != null) {
				if (position == selectedIndex)
					v.setBackgroundColor(pressedColor);
				else
					v.setBackgroundColor(defaultColor);
			}
			return v;
		}
	}

}
